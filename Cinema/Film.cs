﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class Film
    {
        public string Name { get; }
        public TimeSpan Duration { get; }
        public int Price { get; }

        public Film(string name, TimeSpan duration, int price = 150) : this(name, duration.Hours, duration.Minutes, duration.Seconds, price)
        {
        }

        public Film(string name, int hours = 2, int minutes = 30, int seconds = 0, int price = 150)
        {
            Name = name;
            Price = price;
            Duration = new TimeSpan(hours, minutes, seconds);
        }

    }
}
