﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    abstract class Hall : Label
    {
        public List<Place> places { get; protected set; } = new List<Place>();
        public Place currentPlace { get; protected set; }
        public int PlaceCount { get => places.Count; }
        public Label BuyTicketButton { get; } = new Label();
        public Label BookTicketButton { get; } = new Label();

        public Hall() : base()
        {
            BackColor = System.Drawing.Color.Gray;
            BuyTicketButton.Text = "Купить билет на это место";
            BuyTicketButton.Visible = false;
            BuyTicketButton.Size = new System.Drawing.Size(100, 50);
            BuyTicketButton.Location = new System.Drawing.Point(0, 0);
            BuyTicketButton.BackColor = System.Drawing.Color.Aquamarine;
            BuyTicketButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            BuyTicketButton.Parent = this;
            BuyTicketButton.MouseEnter += (object sender, EventArgs e) => { BuyTicketButton.BackColor = System.Drawing.Color.AntiqueWhite; };
            BuyTicketButton.MouseLeave += (object sender, EventArgs e) => { BuyTicketButton.BackColor = System.Drawing.Color.Aquamarine; };
            BuyTicketButton.MouseDown += (object sender, MouseEventArgs e) => { BuyTicketButton.BackColor = System.Drawing.Color.Bisque; };
            BuyTicketButton.MouseUp += (object sender, MouseEventArgs e) => { BuyTicketButton.BackColor = System.Drawing.Color.Aquamarine; };


            BookTicketButton.Text = "Забронировать это место";
            BookTicketButton.Visible = false;
            BookTicketButton.Size = new System.Drawing.Size(100, 50);
            BookTicketButton.Location = new System.Drawing.Point(0, 0);
            BookTicketButton.BackColor = System.Drawing.Color.Aquamarine;
            BookTicketButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            BookTicketButton.Parent = this;
            BookTicketButton.MouseEnter += (object sender, EventArgs e) => { BookTicketButton.BackColor = System.Drawing.Color.AntiqueWhite; };
            BookTicketButton.MouseLeave += (object sender, EventArgs e) => { BookTicketButton.BackColor = System.Drawing.Color.Aquamarine; };
            BookTicketButton.MouseDown += (object sender, MouseEventArgs e) => { BookTicketButton.BackColor = System.Drawing.Color.Bisque; };
            BookTicketButton.MouseUp += (object sender, MouseEventArgs e) => { BookTicketButton.BackColor = System.Drawing.Color.Aquamarine; };
        }
        

        protected void connectPlaces()
        {
            foreach (Place place in places)
            {
                place.Click += (object sender, EventArgs e) => {
                    if (!((Place)sender).isSold)
                    {
                        if (!BuyTicketButton.Visible) BuyTicketButton.Visible = true;
                        if (!BookTicketButton.Visible) BookTicketButton.Visible = true;
                        currentPlace = (Place)sender;
                    }
                };
            }
        }
    }

    class BigHall : Hall
    {
        public BigHall() : base()
        {

            BuyTicketButton.Size = new System.Drawing.Size(248, 20);
            BuyTicketButton.Location = new System.Drawing.Point(0, 364);
            BookTicketButton.Size = new System.Drawing.Size(248, 20);
            BookTicketButton.Location = new System.Drawing.Point(248, 364);
            Size = new System.Drawing.Size(496, 384);
            for(int i = 0; i< 6; i++)
                for(int j =0; j <2; j++)
                {
                    Place tempPlace = new VIP_Place()
                    {
                        Location = new System.Drawing.Point(j * (32 + 48 * 9), 32 + i * (32 + 16)),
                        Parent = this,
                        Number = i*2 + j
                    };
                    places.Add(tempPlace);
                }
            for (int i = 0; i < 6; i++)
                for(int j = 0; j < 8; j++)
                {
                    Place tempPlace = new Place()
                    {
                        Location = new System.Drawing.Point(64 + j * (32 + 16), 32 + i * (32 + 16)),
                        Parent = this,
                        Number = 12+(i*8 + j)
                    };
                    places.Add(tempPlace);
                }
            for(int i = 0; i < 6; i++)
            {
                Place tempPlace = new Love_Place()
                {
                    Location = new System.Drawing.Point(16 + i * (64 + 16), 320),
                    Parent = this,
                    Number = 60 + i
                };
                places.Add(tempPlace);
            }
            connectPlaces();
        }
    }

    class SmallHall : Hall
    {
        public SmallHall() : base()
        {

            BuyTicketButton.Size = new System.Drawing.Size(128, 20);
            BuyTicketButton.Location = new System.Drawing.Point(0, 238);
            BookTicketButton.Size = new System.Drawing.Size(128, 20);
            BookTicketButton.Location = new System.Drawing.Point(128, 238);
            Size = new System.Drawing.Size(256, 258);
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 2; j++)
                {
                    Place tempPlace = new VIP_Place()
                    {
                        Location = new System.Drawing.Point(j * (32 + 48 * 4), 32 + i * (32 + 16)),
                        Parent = this,
                        Number = i * 2 +j
                    };
                    places.Add(tempPlace);
                }
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    Place tempPlace = new Place()
                    {
                        Location = new System.Drawing.Point(64 + j * (32 + 16), 32 + i * (32 + 16)),
                        Parent = this,
                        Number = 6+(i*3 + j)
                    };
                    places.Add(tempPlace);
                }
            for (int i = 0; i < 3; i++)
            {
                Place tempPlace = new Love_Place()
                {
                    Location = new System.Drawing.Point(16 + i * (64 + 16), 196),
                    Parent = this,
                    Number = 15 + i
                };
                places.Add(tempPlace);
            }
            connectPlaces();
        }
    }
}
