﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    class Ticket : FlowLayoutPanel
    {
        public int Price { get; }
        public int Place { get => place.Number; }
        public string Film { get => film.Name; }
        public DateTime Date { get; }
        public TimeSpan Duration { get => film.Duration; }
        private Film film;
        public Place place { get; }

        public Ticket(Session session, Place place) : base()
        {
            string str = "";
            film = session.Film;
            Date = session.Time;
            this.place = place;
            Size = new System.Drawing.Size(450, 75);
            BorderStyle = BorderStyle.FixedSingle;
            Price = (int)(film.Price * place.PriceImpact * session.PriceImpact);

            Label FilmNameLabel = new Label();
            FilmNameLabel.AutoSize = true;
            FilmNameLabel.Text = "Название фильма: " + Film;
            FilmNameLabel.Parent = this;

            Label DateLabel = new Label();
            DateLabel.AutoSize = true;
            DateLabel.Text = "Время: " + Date.ToShortTimeString();
            DateLabel.Parent = this;

            Label DurationLabel = new Label();
            DurationLabel.AutoSize = true;
            DurationLabel.Text = "Длительность: " + Duration.ToString();
            DurationLabel.Parent = this;

            Label PlaceLabel = new Label();
            PlaceLabel.AutoSize = true;

            PlaceLabel.Text = "Место: " + Place.ToString();
            PlaceLabel.Parent = this;

            Label PriceLabel = new Label();
            PriceLabel.AutoSize = true;
            PriceLabel.Text = "Цена: " + Price.ToString();
            PriceLabel.Parent = this;

        }
    }
}
