﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    public partial class Form1 : Form
    {
        Hall currentHall = null;
        Ticket currentTicket = null;
        List<Session> sessions = new List<Session>();
        Film film1 = new Film("Холодное сердце 2");
        Film film2 = new Film("Шрек");
        public Form1()
        {
            InitializeComponent();
            sessions.Add(new Session_2D(film1, 12, 30));
            sessions.Add(new Session_3D(film1, 14, 30));
            sessions.Add(new Session_2D(film2, 15, 0));
            sessions.Add(new Session_IMAX(film2, 16, 0));

            foreach (Session session in sessions)
            {
                session.on_PlaceClicked += ChangeTicket;
                session.on_Clicked += ChangeHall;
                session.Parent = flowLayoutPanel1;
            }
        }

        void ChangeTicket(Ticket ticket)
        {
            if (currentTicket != null) currentTicket.Parent = null;
            currentTicket = ticket;
            ticket.Parent = panel3;
        }
        void ChangeHall(Hall hall)
        {
            if (currentHall != null) currentHall.Parent = null;
            if (currentTicket != null) currentTicket.Parent = null;
            currentHall = hall;
            hall.Parent = panel2;
        }
    }
}
