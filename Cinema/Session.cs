﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    abstract class Session : Label
    {
        public DateTime Time { get; }
        public Film Film { get; }
        public Hall Hall { get; protected set; }
        public List<Ticket> Tickets { get; protected set; } = new List<Ticket>();


        public delegate void on_PlaceClickedDeligat(Ticket ticket);
        public on_PlaceClickedDeligat on_PlaceClicked;
        public delegate void on_ClickedDeligat(Hall hall);
        public on_ClickedDeligat on_Clicked;

        public Session(Film film)
            : this (film, DateTime.Now.Hour, DateTime.Now.Minute) { }

        public Session(Film film, int hour, int minute)
            : this(film, DateTime.Now.Day, hour, minute) { }

        public Session(Film film, int day, int hour, int minute)
            : this(film, DateTime.Now.Month, day, hour, minute) { }

        public Session(Film film, int month, int day, int hour, int minute)
            : this(film, DateTime.Now.Year, month, day, hour, minute) { }

        public Session(Film film, DateTime time)
            : this(film, time.Year, time.Month, time.Day, time.Hour, time.Minute) { }


        public Session(Film film, int year, int month, int day, int hour, int minute) : base()
        {
            Film = film;
            Time = new DateTime(year, month, day, hour, minute, 0);
            Click += (object sender, EventArgs e) => { on_Clicked(Hall); };
            MouseEnter += On_MouseEnter;
            MouseLeave += On_MouseLeave;
            MouseDown += On_MouseDown;
            MouseUp += On_MouseUp;

            BackColor = System.Drawing.Color.DarkGreen;
            Size = new System.Drawing.Size(250, 100);
            Text = "Фильм: " + Film.Name +
                "\nДата: " + Time.ToLongDateString() +
                "\nВремя: " + Time.ToShortTimeString();
            Margin = new Padding(5);
        }




        protected void BuyTicket(object sender, EventArgs e)
        {
            foreach (Ticket ticket in Tickets)
            {
                if(Hall.currentPlace != null && ticket.place == Hall.currentPlace)
                {
                    Hall.currentPlace.Sell();
                    Hall.BuyTicketButton.Visible = false;
                    Hall.BookTicketButton.Visible = false;
                }
            }
        }

        protected void BookTicket(object sender, EventArgs e)
        {
            foreach (Ticket ticket in Tickets)
            {
                if (Hall.currentPlace != null && ticket.place == Hall.currentPlace)
                {
                    Hall.currentPlace.Book();
                    Hall.BuyTicketButton.Visible = false;
                    Hall.BookTicketButton.Visible = false;
                }
            }
        }

        protected void if_PlacesClicked(object sender, EventArgs e)
        {
            if (((Place)sender).isSold) return;
            foreach (Ticket ticket in Tickets)
            {
                if (ticket.place == ((Place)sender))
                {
                    on_PlaceClicked(ticket);
                }
            }
        }


        virtual protected void On_MouseLeave(object sender, EventArgs e)
        {
            BackColor = System.Drawing.Color.DarkGreen;
        }

        virtual protected void On_MouseEnter(object sender, EventArgs e)
        {
            BackColor = System.Drawing.Color.Green;
        }

        virtual protected void On_MouseDown(object sender, EventArgs e)
        {
             BackColor = System.Drawing.Color.LightGreen;
        }

        virtual protected void On_MouseUp(object sender, EventArgs e)
        {
            BackColor = System.Drawing.Color.Green;
        }

        

        public virtual float PriceImpact => 0.0f;
    }

    class Session_2D : Session
    {
        public Session_2D(Film film)
            : this(film, DateTime.Now.Hour, DateTime.Now.Minute) { }

        public Session_2D(Film film, int hour, int minute)
            : this(film, DateTime.Now.Day, hour, minute) { }

        public Session_2D(Film film, int day, int hour, int minute)
            : this(film, DateTime.Now.Month, day, hour, minute) { }

        public Session_2D(Film film, int month, int day, int hour, int minute)
            : this(film, DateTime.Now.Year, month, day, hour, minute) { }

        public Session_2D(Film film, DateTime time)
            : this(film, time.Year, time.Month, time.Day, time.Hour, time.Minute) { }


        public Session_2D(Film film, int year, int month, int day, int hour, int minute)
            : base(film, year, month, day, hour, minute)
        {
            Hall = new BigHall();
            foreach (Place place in Hall.places)
            {
                Tickets.Add(new Ticket(this, place));
                place.Click += if_PlacesClicked;
            }

            Hall.BuyTicketButton.Click +=  BuyTicket;
            Hall.BookTicketButton.Click += BookTicket;
            Text += "\nТип: 2D";
        }

        override public float PriceImpact => 1.0f;


    }

    class Session_3D : Session
    {
        public Session_3D(Film film)
            : this(film, DateTime.Now.Hour, DateTime.Now.Minute) { }

        public Session_3D(Film film, int hour, int minute)
            : this(film, DateTime.Now.Day, hour, minute) { }

        public Session_3D(Film film, int day, int hour, int minute)
            : this(film, DateTime.Now.Month, day, hour, minute) { }

        public Session_3D(Film film, int month, int day, int hour, int minute)
            : this(film, DateTime.Now.Year, month, day, hour, minute) { }

        public Session_3D(Film film, DateTime time)
            : this(film, time.Year, time.Month, time.Day, time.Hour, time.Minute) { }


        public Session_3D(Film film, int year, int month, int day, int hour, int minute)
            : base(film, year, month, day, hour, minute)
        {
            Hall = new BigHall();
            foreach (Place place in Hall.places)
            {
                Tickets.Add(new Ticket(this, place));
                place.Click += if_PlacesClicked;
            }

            Hall.BuyTicketButton.Click += BuyTicket;
            Hall.BookTicketButton.Click += BookTicket;
            Text += "\nТип: 3D";
        }
        override public float PriceImpact => 1.5f;



    }

    class Session_IMAX : Session
    {
        public Session_IMAX(Film film)
            : this(film, DateTime.Now.Hour, DateTime.Now.Minute) { }

        public Session_IMAX(Film film, int hour, int minute)
            : this(film, DateTime.Now.Day, hour, minute) { }

        public Session_IMAX(Film film, int day, int hour, int minute)
            : this(film, DateTime.Now.Month, day, hour, minute) { }

        public Session_IMAX(Film film, int month, int day, int hour, int minute)
            : this(film, DateTime.Now.Year, month, day, hour, minute) { }

        public Session_IMAX(Film film, DateTime time)
            : this(film, time.Year, time.Month, time.Day, time.Hour, time.Minute) { }


        public Session_IMAX(Film film, int year, int month, int day, int hour, int minute)
            : base(film, year, month, day, hour, minute)
        {
            Hall = new SmallHall();
            foreach (Place place in Hall.places)
            {
                Tickets.Add(new Ticket(this, place));
                place.Click += if_PlacesClicked;
            }

            Hall.BuyTicketButton.Click += BuyTicket;
            Hall.BookTicketButton.Click += BookTicket;
            Text += "\nТип: IMAX";
        }
        override public float PriceImpact => 2.0f;


    }
}
