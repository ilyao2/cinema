﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    class Place : Label
    {

        public bool isSold { get; protected set; } = false;
        public int Number { get; set; }
        private static int idCounter = 0;
        public Place() : base()
        {
            Number = idCounter++;
            Size = new System.Drawing.Size(32, 32);
            BackColor = System.Drawing.Color.Blue;
            Click += On_Click;
            MouseEnter += On_MouseEnter;
            MouseLeave += On_MouseLeave;
            MouseDown += On_MouseDown;
            MouseUp += On_MouseUp;
        }

        public virtual float PriceImpact => 1.0f;
        public float Sell()
        {
            isSold = true;
            BackColor = System.Drawing.Color.DarkGray;
            return PriceImpact;
        }
        public float Book()
        {
            isSold = true;
            BackColor = System.Drawing.Color.Red;
            return PriceImpact;
        }

        virtual protected void On_MouseLeave(object sender, EventArgs e)
        {
            if(!isSold)
                BackColor = System.Drawing.Color.Blue;
        }

        virtual protected void On_MouseEnter(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightBlue;
        }

        virtual protected void On_MouseDown(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.DarkBlue;
        }

        virtual protected void On_MouseUp(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightBlue;
        }

        virtual protected void On_Click(object sender, EventArgs e)
        {

        }

    }

    class VIP_Place : Place
    {
        public VIP_Place() : base()
        {
            BackColor = System.Drawing.Color.Yellow;
        }
        override public float PriceImpact => 2.0f;

        override protected void On_MouseLeave(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.Yellow;
        }

        override protected void On_MouseEnter(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightYellow;
        }
        override protected void On_MouseDown(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.DarkOrange;
        }
        override protected void On_MouseUp(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightYellow;
        }
    }
    
    class Love_Place : Place
    {
        public Love_Place() : base()
        {
            BackColor = System.Drawing.Color.Green;
            Size = new System.Drawing.Size(64, 32);
        }
        public override float PriceImpact => 1.5f;

        override protected void On_MouseLeave(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.Green;
        }

        override protected void On_MouseEnter(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightGreen;
        }

        override protected void On_MouseDown(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.DarkGreen;
        }
        override protected void On_MouseUp(object sender, EventArgs e)
        {
            if (!isSold)
                BackColor = System.Drawing.Color.LightGreen;
        }
    }
}
